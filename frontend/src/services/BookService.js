import axios from 'axios';

const API_URL = "http://localhost:8080"; 

class BookService {

    authHeader = () => {
        let user = JSON.parse(localStorage.getItem('user'));
        return user ? user.authHeader : "";
    }

    getBooks() {
        return axios.get(API_URL + '/books', {headers: {authorization:  'Basic ' + this.authHeader()}});
    }

    getBookById(id) {
        return axios.get(API_URL + '/books/' + id, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    addBook(book) {
        return axios.post(API_URL + '/books', book, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    updateBook(book, bookId) {
        return axios.put(API_URL + '/books/' + bookId, book, {headers: {authorization: 'Basic ' + this.authHeader()}});
    } 

    deleteBook(bookId) {
        return axios.delete(API_URL + '/books/' + bookId, {headers: {authorization: 'Basic ' + this.authHeader()}});
    } 

    getReviewsByBookId(bookId) {
        return axios.get(API_URL + '/books/' + bookId + '/reviews', {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    addReviewByBookId(review, bookId) {
        return axios.post(API_URL + '/books/' + bookId + '/reviews', review, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    updateReviewByBookId(review, bookId, reviewId) {
        return axios.put(API_URL + '/books/' + bookId + '/reviews/' + reviewId, review, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    deleteReviewByBookIdReviewId(bookId,  reviewId) {
        return axios.delete(API_URL + '/books/' + bookId + '/reviews/' + reviewId, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    getUsers() {
        return axios.get(API_URL + '/users', {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

    addUser(newUser) {
        return axios.post(API_URL + '/users', newUser, {headers: {authorization: 'Basic ' + this.authHeader()}});
    }

}

export default new BookService()