import React, { Component } from 'react';
import BookService from '../services/BookService';
import 'bootstrap/dist/css/bootstrap.min.css';


class ListBooks extends Component {
    constructor(props) {
        super(props);

        this.state = {
            books:[],
        };

        this.addBook = this.addBook.bind(this);
    }

    componentDidMount() {
        BookService.getBooks()
            .then((res) => {
                this.setState({ books:res.data });
            })
            .catch(() => {
                this.props.history.push('/error');
            });
    }
    
    addBook() {
        this.props.history.push('/add-book');
    }

    updateBook(id) {
        this.props.history.push(`/update-book/${id}`);
    }

    deleteBook(id) {
        BookService.deleteBook(id)
            .then((res) => {
                this.setState({books: this.state.books.filter(book => book.id !== id)});
            })
            .catch(() => {
                this.props.history.push('/error');
            });
    }

    getReviewsByBookId(id) {
        this.props.history.push(`/list-reviews/${id}`);
    }

    render() {

        return (
            <div>
                <br></br>
                <div>
                    <button className="btn btn-primary" onClick={this.addBook}>Add Book</button>
                </div>
                <br></br>
                <div className = "row">
                    <table className = "table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Reviews</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.books.map(
                                    book =>
                                    <tr key = {book.id}>
                                        <td>{book.id}</td>
                                        <td>{book.title}</td>
                                        <td>
                                            <button onClick={()=>this.getReviewsByBookId(book.id)} className="btn btn-info">View</button>
                                        </td>
                                        <td>
                                            <button onClick={()=>this.updateBook(book.id)} className="btn btn-info">Update</button>
                                            <button style={{marginLeft: "10px"}} onClick={()=>this.deleteBook(book.id)} className="btn btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );

    }

}

export default ListBooks