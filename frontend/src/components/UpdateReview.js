import React, { Component } from 'react';
import BookService from '../services/BookService';

class UpdateReview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bookId: this.props.match.params.bookId,
            reviewId: this.props.match.params.reviewId,
            review: ''
        };

        this.changeReviewHandler = this.changeReviewHandler.bind(this);
        this.saveReview = this.saveReview.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    componentDidMount() {
        console.log(typeof(this.state.bookId));
        BookService.getReviewsByBookId(this.state.bookId).then((res) => {
            let reviews = res.data;
            this.setState({review: reviews.find(review => review.id === parseInt(this.state.reviewId)).review});
        });
    }


    changeReviewHandler = (event) => {
        this.setState({review: event.target.value});
    }

    saveReview = (event) => {
        event.preventDefault();
        let review= {review: this.state.review};
        let bookId = this.state.bookId;
        let reviewId = this.state.reviewId;
        BookService.updateReviewByBookId(review, bookId, reviewId)
            .then(res => {
                this.props.history.push(`/list-reviews/${bookId}`);
            })
            .catch(() => {
                this.props.history.push('/error');
            });
    }

    cancel() {
        let bookId = this.state.bookId;
        this.props.history.push(`/list-reviews/${bookId}`);
    }

    render() {
        return (
            <div className="container">
                <br></br>
                <div className="card col-md-6 offset-md-3 offset-md-3">
                    <h4 className="text-center">Update Review</h4>
                    <div className = "card-body">
                        <form>
                            <div>
                                <label>Review</label>
                                <input placeholder="review" name="review" className="form-control" value={this.state.review} onChange={this.changeReviewHandler}/>
                            </div>
                            <br></br>
                            <button className="btn btn-success" onClick={this.saveReview}>Save</button>
                            <button className="btn btn-danger" onClick={this.cancel} style={{marginLeft: "10px"}}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
            
        );
    }
}

export default UpdateReview;