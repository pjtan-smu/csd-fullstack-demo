import React, { Component } from 'react';
import BookService from '../services/BookService';

class AddReview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bookId: this.props.match.params.bookId,
            review: ''
        };

        this.changeReviewHandler = this.changeReviewHandler.bind(this);
        this.saveReview = this.saveReview.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    changeReviewHandler = (event) => {
        this.setState({review: event.target.value});
    }

    saveReview = (event) => {
        event.preventDefault();
        let review= {review: this.state.review};
        let bookId = this.state.bookId;
        BookService.addReviewByBookId(review, bookId)
            .then(res => {
                this.props.history.push(`/list-reviews/${bookId}`);
            })
            .catch(() => {
                this.props.history.push('/error');
            });
    }

    cancel() {
        let bookId = this.state.bookId;
        this.props.history.push(`/list-reviews/${bookId}`);
    }

    render() {
        return (
            <div className="container">
                <br></br>
                <div className="card col-md-6 offset-md-3 offset-md-3">
                    <h4 className="text-center">Add Review</h4>
                    <div className = "card-body">
                        <form>
                            <div>
                                <label>Review</label>
                                <input placeholder="review" name="review" className="form-control" value={this.state.review} onChange={this.changeReviewHandler}/>
                            </div>
                            <br></br>
                            <button className="btn btn-success" onClick={this.saveReview}>Save</button>
                            <button className="btn btn-danger" onClick={this.cancel} style={{marginLeft: "10px"}}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
            
        );
    }
}

export default AddReview;