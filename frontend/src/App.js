import 'bootstrap/dist/css/bootstrap.min.css';
import ListBooks from './components/ListBooks.js';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import Header from './components/Header.js';
import AddBook from './components/AddBook.js';
import UpdateBook from './components/UpdateBook.js';
import ListReviews from './components/ListReviews.js';
import AddReview from './components/AddReview.js';
import UpdateReview from './components/UpdateReview.js';
import ListUsers from './components/ListUsers.js';
import AddUser from './components/AddUser.js';
import Login from './components/Login.js';
import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Logout from './components/Logout.js';
import Error from './components/Error.js';


class App extends Component {


  render() {
    return (
        <Container>
          <Router>
              <Header />
              <Switch> 
                <Route path = "/list-books" component = {ListBooks}></Route>
                <Route path = "/add-book" component = {AddBook}></Route>
                <Route path = "/update-book/:id" component = {UpdateBook}></Route>
                <Route path = "/list-reviews/:bookId" component = {ListReviews}></Route>
                <Route path = "/add-review/:bookId" component = {AddReview}></Route>
                <Route path = "/update-review/:bookId/:reviewId" component = {UpdateReview}></Route>
                <Route path = "/list-users" component = {ListUsers}></Route>
                <Route path = "/add-user" component = {AddUser}></Route>
                <Route path = "/login" component={Login}/>
                <Route path = "/logout" component = {Logout}></Route>
                <Route path = "/error" component = {Error}></Route>
              </Switch>
          </Router>
        </Container>
    );
  }


}

export default App;